// slider
$(function() {
    $('#slides').slidesjs({

        width: 940,
        height: 528,
        navigation: {
        effect: "fade"
        },
        pagination: {
            effect: "fade"
        },
        effect: {
            fade: {
                speed: 400
            }
        }

    });
});
// toggle
$(document).ready(function(){
    $('.clickme').each(function() {
        $(this).on('click', function(e){
            e.preventDefault();
            if (!$(this).hasClass('up')) {
                $('.tab-block').find('.up').removeClass('up');
                $(this).addClass('up');
            }
            $box = $(this).next('.box');
            if(!$box.hasClass('opened'))
            {
                $('.tab-block').find('.opened').removeClass('opened').slideUp('slow');
                $box.addClass('opened').slideDown('slow');
            }
        });
    });
});

// popup form

    $('#trigger-modal').click(function(){
        $('#screen, #modal').show();
    });

    $('#modal button').click(function(){
        $('#screen, #modal').hide();
    });

    $('#trigger-modal2').click(function(){
        $('#screen2, #modal2').show();
    });

    $('#modal2 button').click(function(){
        $('#screen2, #modal2').hide();
    });

//datapicker

$(document).ready(function(){
    $( "#datepicker, #datepicker1" ).datepicker();
  });



