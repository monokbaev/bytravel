from django.shortcuts import render_to_response
from django.views.generic import ListView, DetailView
from hotel.models import Category
from slider.models import Slider
from byTravel.models import *
from navbar.models import Navbar, NavbarEn
# Create your views here.
class IndexView(ListView):
    model = Category
    template_name = 'main.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['sliders'] = Slider.objects.all()
        context['socials'] = Social.objects.all()
        context['addresses'] = Address.objects.all()
        context['phones'] = Phone.objects.all()
        context['mails'] = Mail.objects.all()
        context['navbars'] = Navbar.objects.all()
        context['main_text'] = MainText.objects.get()
        context['gmeta'] = GlobalMeta.objects.filter(title="global")

        return context
class AboutView(IndexView):
    template_name = 'byTravel/about.html'


    def get_context_data(self, **kwargs):
        context = super(AboutView, self).get_context_data(**kwargs)
        context['about'] = About.objects.get(pk = 2)
        context['gmeta'] = GlobalMeta.objects.filter(title="global")
        context['lmeta'] = GlobalMeta.objects.filter(title="about")

        return context

class TransferView(IndexView):
    template_name = 'byTravel/transfer.html'


    def get_context_data(self, **kwargs):
        context = super(TransferView, self).get_context_data(**kwargs)
        context['transfer'] = About.objects.get(pk = 1)
        context['gmeta'] = GlobalMeta.objects.filter(title="global")
        context['lmeta'] = GlobalMeta.objects.filter(title="transfer")
        return context


