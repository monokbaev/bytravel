# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import froala_editor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('byTravel', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='About',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='Title')),
                ('text', froala_editor.fields.FroalaField(verbose_name='Text')),
                ('image', models.ImageField(upload_to='uploads/about/', verbose_name='Image')),
            ],
            options={
                'db_table': 'about',
                'verbose_name': 'Page',
                'verbose_name_plural': 'Pages',
            },
        ),
    ]
