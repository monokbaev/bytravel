# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('address', models.CharField(max_length=255, verbose_name='Address')),
            ],
            options={
                'db_table': 'address',
                'verbose_name': 'Address',
                'verbose_name_plural': 'Addresses',
            },
        ),
        migrations.CreateModel(
            name='Mail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('mail', models.CharField(max_length=255, verbose_name='Mail')),
            ],
            options={
                'db_table': 'mail',
                'verbose_name': 'Mail',
                'verbose_name_plural': 'Mails',
            },
        ),
        migrations.CreateModel(
            name='Phone',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('phone', models.CharField(max_length=255, verbose_name='Phone')),
            ],
            options={
                'db_table': 'phone',
                'verbose_name': 'Telephone',
                'verbose_name_plural': 'Telephons',
            },
        ),
        migrations.CreateModel(
            name='Social',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='Title')),
                ('url', models.CharField(max_length=255, verbose_name='Url')),
                ('icon', models.ImageField(upload_to='uploads/social', verbose_name='Image')),
            ],
            options={
                'db_table': 'social',
                'verbose_name': 'Social',
                'verbose_name_plural': 'Socials',
            },
        ),
    ]
