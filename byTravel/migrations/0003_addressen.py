# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-03-11 05:30
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('byTravel', '0002_about'),
    ]

    operations = [
        migrations.CreateModel(
            name='AddressEn',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('address', models.CharField(max_length=255, verbose_name='\u0410\u0434\u0440\u0435\u0441 \u0434\u043b\u044f \u0430\u043d\u0433\u043b\u0438\u0439\u0441\u043a\u043e\u0439 \u0432\u0435\u0440\u0441\u0438\u0438')),
                ('belongs_to', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='byTravel.Address')),
            ],
            options={
                'verbose_name_plural': '\u0410\u0434\u0440\u0435\u0441 \u0434\u043b\u044f \u0430\u043d\u0433\u043b\u0438\u0439\u0441\u043a\u043e\u0439 \u0432\u0435\u0440\u0441\u0438\u0438',
            },
        ),
    ]
