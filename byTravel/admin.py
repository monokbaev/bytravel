from django.contrib import admin
from .models import *
# Register your models here.

class AddressEnInline(admin.StackedInline):
    model = AddressEn
    extra = 1


class AddressInline(admin.ModelAdmin):
    inlines = [AddressEnInline, ]



class AboutEnInline(admin.StackedInline):
    model = AboutEn
    extra = 1

class AboutInline(admin.ModelAdmin):
    inlines = [AboutEnInline, ]


class MainTextEnInline(admin.StackedInline):
    model = MainTextEn
    extra = 1

class MainTextInline(admin.ModelAdmin):
    inlines = [MainTextEnInline, ]


admin.site.register(AddressEn)
admin.site.register(Social)
admin.site.register(Phone)
admin.site.register(Mail)
admin.site.register(Address, AddressInline)
admin.site.register(About, AboutInline)
admin.site.register(AboutEn)
admin.site.register(MainText, MainTextInline)
admin.site.register(MainTextEn)
admin.site.register(LocalMeta)
admin.site.register(GlobalMeta)