from django.conf.urls import url
from .views import *

app_name = 'main'
urlpatterns = [
    url(r'about/$', AboutView.as_view(), name = 'about'),
    url(r'transfer/$', TransferView.as_view(), name = 'transfer'),
    url(r'^$', IndexView.as_view(), name = 'index')
]