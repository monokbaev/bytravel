# coding: utf-8
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.db import models
from froala_editor.fields import FroalaField
from hotel.models import *

# Create your models here.

class Social(models.Model):
    title = models.CharField(verbose_name = _('Title'), max_length = 255)
    url = models.CharField(verbose_name = _('Url'), max_length = 255)
    icon = models.ImageField(verbose_name = _('Image'), upload_to = ('uploads/social'))
    icon_hover = models.ImageField(verbose_name = _('Image hover'), upload_to = ('uploads/social'), default=None)

    def __unicode(self):
        return self.title

    class Meta:
        db_table = 'social'
        verbose_name = _('Social')
        verbose_name_plural = _('Socials')

class Phone(models.Model):
    phone = models.CharField(verbose_name = _('Phone'), max_length = 255)

    def __unicode(self):
        return self.phone

    class Meta:
        db_table = 'phone'
        verbose_name = _('Telephone')
        verbose_name_plural = _('Telephons')

class Mail(models.Model):
    mail = models.CharField(verbose_name = _('Mail'), max_length = 255)

    def __unicode(self):
        return self.mail

    class Meta:
        db_table = 'mail'
        verbose_name = _('Mail')
        verbose_name_plural = _('Mails')

class Address(models.Model):
    address = models.CharField(verbose_name = _('Address'), max_length = 255)

    def __unicode(self):
        return self.address

    class Meta:
        db_table = 'address'
        verbose_name = _('Address')
        verbose_name_plural = _('Addresses')


class AddressEn(models.Model):
    class Meta:
        verbose_name_plural = "Адрес для английской версии"

    address = models.CharField(verbose_name="Адрес для английской версии", max_length=255)
    belongs_to = models.ForeignKey(Address)

    def __unicode__(self):
        return self.address


class About(models.Model):
    title = models.CharField(verbose_name = _('Title'), max_length = 255)
    text = FroalaField(verbose_name = _('Text'))
    image = models.ImageField(verbose_name = _('Image'), upload_to = 'uploads/about/')
    price = models.FileField(verbose_name="Прайс-лист", upload_to='uploads/about/', default=None, blank=True, null=True)

    def __unicode__(self):
        return self.title

    class Meta:
        db_table = 'about'
        verbose_name_plural = "О нас"


class AboutEn(models.Model):
    title = models.CharField(verbose_name = _('Title'), max_length = 255)
    text = FroalaField(verbose_name = _('Text'))
    image = models.ImageField(verbose_name = _('Image'), upload_to = 'uploads/about/')
    price = models.FileField(verbose_name="Прайс-лист", upload_to='uploads/about/', default=None, blank=True, null=True)
    belongs_to = models.ForeignKey(About)

    def __unicode__(self):
        return self.title

    class Meta:
        db_table = 'about_en'
        verbose_name_plural = "О нас на английском"


class MainText(models.Model):
    class Meta:
        verbose_name_plural = "Текст на главной"

    title = models.CharField(max_length=55, verbose_name="Заголовок")
    text = FroalaField(verbose_name="Текст")

    def __unicode__(self):
        return self.title


class MainTextEn(models.Model):
    class Meta:
        verbose_name_plural = "Текст на главной на англиском"

    title = models.CharField(max_length=55, verbose_name="Заголовок")
    text = FroalaField(verbose_name="Текст")
    belongs_to = models.ForeignKey(MainText)

    def __unicode__(self):
        return self.title


class GlobalMeta(models.Model):
    class Meta:
        verbose_name_plural = "Глобальные мета теги"

    title = models.CharField(max_length=55, default=None, help_text="Для указания локальной мета-информации на странице зимнего отдыха укажите здесь 'winter', для страницы летнего отдыха 'summer', для страницы контактов 'contacts', для трансферов 'transfer', а для глобального 'global', для страницы о нас 'about' ")
    meta_title = models.CharField(max_length = 100, verbose_name = _('Мета header'))
    meta_desc = models.TextField(verbose_name = _('Мета description'))
    meta_key = models.CharField(max_length = 200, verbose_name = ('Мета keywords'))

    def __unicode__(self):
        return self.title


class LocalMeta(models.Model):
    class Meta:
        verbose_name_plural = "Локальные мета-теги для каждого отеля"

    meta_title = models.CharField(max_length = 100, verbose_name = _('Мета header'))
    meta_desc = models.TextField(verbose_name = _('Мета description'))
    meta_key = models.CharField(max_length = 200, verbose_name = ('Мета keywords'))
    hotel = models.ForeignKey(Hotel, verbose_name="Для какого отеля мета-теги?")

    def __unicode__(self):
        return self.meta_title
