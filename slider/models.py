from django.db import models
from django.utils.translation import ugettext_lazy as _
# Create your models here.
class Slider(models.Model):
    title = models.CharField(verbose_name = _('Title'), max_length = 255)
    image = models.ImageField(verbose_name = _('Image'), upload_to = ('uploads/slider'))

    def __unicode__(self):
        return self.title

    class Meta:
        db_table = 'slider'
        verbose_name = _('Slider')
        verbose_name_plural = _('Sliders')
