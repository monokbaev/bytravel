# coding: utf-8
from django.core.mail import send_mail
from django.shortcuts import render, HttpResponse, render_to_response
from django.template.context_processors import csrf
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext as _
from .models import Order
from django.views.generic import View
# Create your views here.
class OrderView(View):

    def post(self, request):
        if request.method == 'POST':
            name = request.POST.get('name')
            email = request.POST.get('email')
            phone = request.POST.get('phone')
            try:
                deal = request.POST.get('deal')
            except ValidationError(_("Введите целое число")):
                deal = False
            d_from = request.POST.get('from')
            d_to = request.POST.get('to')
            room = request.POST.get('room')
            info = request.POST.get('info')
            order = Order(name = name, email = email, phone = phone, deal = deal, d_from = d_from, d_to = d_to, room = room, info = info)
            order.save()
            name = name.encode('utf-8')
            name = 'Мое имя: ' + str(name) + '\n'
            phone = 'Мой номер: ' + str(phone) + '\n'
            email = 'Моя почта: ' + str(email) + '\n'
            deal = 'Количество гостей: ' + str(deal) + "\n"
            info = "Дополнительная информация: " + str(info) + '\n'
            d_from = "Дата начала: " + str(d_from) + '\n'
            d_to = "Дата конца: " + str(d_to) + '\n'
            order_id = str(order.id)
            send_mail("Клиент желает забронировать место (ByTravel)", name + phone + email + deal + info + d_from + d_to + 'Заявка номер: ' + order_id, 'monokbaev@gmail.com', ['monokbaev@gmail.com'])
            return HttpResponse('Ваша заявка принята')


def csrf_to_winter_detail(request, context={}):
    context.update(csrf(request))
    return render_to_response('hotel/winter_detail.html', context)


def create_order(request):
    name = request.POST.get('name')
    email = request.POST.get('email')
    phone = request.POST.get('phone')
    deal = request.POST.get('deal')
    info = request.POST.get('info')
    d_from = request.POST.get('from')
    d_to = request.POST.get('to')
    order = Order(name=name, email=email, phone=phone, deal=deal, d_from=d_from, d_to=d_to, room=" ", info=info)
    order.save()
    name = name.encode('utf-8')
    name = 'Мое имя: ' + str(name) + '\n'
    phone = 'Мой номер: ' + str(phone) + '\n'
    email = 'Моя почта: ' + str(email) + '\n'
    deal = 'Количество гостей: ' + str(deal) + "\n"
    info = "Дополнительная информация: " + str(info) + '\n'
    d_from = "Дата начала: " + str(d_from) + '\n'
    d_to = "Дата конца: " + str(d_to) + '\n'
    order_id = str(order.id)
    send_mail("Клиент желает забронировать место (ByTravel)", name + phone + email + deal + info + d_from + d_to + 'Заявка номер: ' + order_id, 'monokbaev@gmail.com', ['monokbaev@gmail.com'])
    return HttpResponse('Ваша заявка принята')


