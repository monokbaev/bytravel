from django.db import models
from django.utils.translation import ugettext_lazy as _
# Create your models here.


class Order(models.Model):
    name = models.CharField(verbose_name = _('Name'), max_length = 255)
    email = models.CharField(verbose_name = _('Email'), max_length = 255)
    phone = models.CharField(verbose_name = _('Phone'), max_length = 255)
    deal = models.IntegerField(verbose_name = _('Guest deal'), default=1)
    d_from = models.CharField(verbose_name = _('From'), max_length = 255)
    d_to = models.CharField(verbose_name = _('To'), max_length = 255)
    room = models.CharField(verbose_name = _('Room'), max_length = 255, null=True)
    info = models.TextField(verbose_name = _('Info'), default=None)

    def __unicode__(self):
        return self.email

    class Meta:
        db_table = 'order'
        verbose_name = _('Order')
        verbose_name_plural = _('Orders')