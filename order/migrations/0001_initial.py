# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('email', models.CharField(max_length=255, verbose_name='Email')),
                ('phone', models.CharField(max_length=255, verbose_name='Phone')),
                ('deal', models.IntegerField(verbose_name='Guest deal')),
                ('d_from', models.DateField(verbose_name='From')),
                ('d_to', models.DateField(verbose_name='To')),
                ('room', models.CharField(max_length=255, verbose_name='Room')),
                ('info', models.TextField(verbose_name='Info')),
            ],
            options={
                'db_table': 'order',
                'verbose_name': 'Order',
                'verbose_name_plural': 'Orders',
            },
        ),
    ]
