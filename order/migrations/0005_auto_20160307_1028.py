# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-03-07 10:28
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0004_auto_20160307_0940'),
    ]

    operations = [
        migrations.RenameField(
            model_name='order',
            old_name='name',
            new_name='username',
        ),
    ]
