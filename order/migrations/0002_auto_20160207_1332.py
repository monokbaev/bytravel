# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='d_from',
            field=models.CharField(max_length=255, verbose_name='From'),
        ),
        migrations.AlterField(
            model_name='order',
            name='d_to',
            field=models.CharField(max_length=255, verbose_name='To'),
        ),
    ]
