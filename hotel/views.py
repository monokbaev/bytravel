from django.shortcuts import render
from django.views.generic import ListView, DetailView
from byTravel.views import IndexView
from .models import *
from byTravel.models import *
from navbar.models import Navbar
# Create your views here.
class WinterListView(IndexView):
    model = Category

    def get_context_data(self, **kwargs):
        context = super(WinterListView, self).get_context_data(**kwargs)
        for item in self.object_list:
            item.hotels = Hotel.objects.all().filter(rest = 'w', category = item)
        context['gmeta'] = GlobalMeta.objects.filter(title="global")
        context['lmeta'] = GlobalMeta.objects.filter(title="winter")
        return context

    template_name = 'hotel/winter.html'

class WinterDetailView(DetailView):
    model = Hotel

    def get_context_data(self, **kwargs):
        context = super(WinterDetailView, self).get_context_data(**kwargs)
        context['images'] = Image.objects.all().filter(hotel = self.object)
        context['tabs'] = Tab.objects.all().filter(hotel = self.object)
        context['hulls'] = Hull.objects.all()
        context['navbars'] = Navbar.objects.all()
        for tab in context['tabs']:
            for hull in context['hulls']:
                hull.count = False
                for hostel in tab.hostel.all():
                    if hostel.hull.id == hull.id:
                        hull.count = True
        context['socials'] = Social.objects.all()
        context['addresses'] = Address.objects.all()
        context['phones'] = Phone.objects.all()
        context['mails'] = Mail.objects.all()
        context['gmeta'] = GlobalMeta.objects.filter(title="global")
        context['lmeta'] = LocalMeta.objects.filter(hotel=self.object)

        return context
    template_name = 'hotel/winter_detail.html'

class SummerListView(IndexView):
    model = Category

    def get_context_data(self, **kwargs):
        context = super(SummerListView, self).get_context_data(**kwargs)
        for item in self.object_list:
            item.hotels = Hotel.objects.all().filter(rest = 's', category = item)
        context['gmeta'] = GlobalMeta.objects.filter(title="global")
        context['lmeta'] = GlobalMeta.objects.filter(title="summer")
        return context

    template_name = 'hotel/summer.html'

