from django.conf.urls import url
from .views import *

app_name = 'hotel'
urlpatterns = [
    url(r'^winter/$', WinterListView.as_view(), name = 'winterList'),
    url(r'^winter/(?P<slug>[-_\w]+)', WinterDetailView.as_view(), name = 'detail'),
    url(r'^summer/', SummerListView.as_view(), name = 'summerList')
]