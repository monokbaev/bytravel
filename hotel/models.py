# coding: utf-8
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.db import models
from froala_editor.fields import FroalaField


# Create your models here.

class Category(models.Model):
    title = models.CharField(verbose_name = _('Title'), max_length = 255)
    text = models.TextField()
    image = models.ImageField(verbose_name = _('Image'), upload_to = 'uploads/category/')

    def __unicode__(self):
        return self.title

    class Meta:
        db_table = 'hotel_category'
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')


class CategoryEn(models.Model):
    title = models.CharField(verbose_name = _('Title'), max_length = 255)
    text = models.TextField()
    image = models.ImageField(verbose_name = _('Image'), upload_to = 'uploads/category/')
    belongs_to = models.ForeignKey(Category)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name_plural = "Категории на английском"


class Hotel(models.Model):
    title = models.CharField(verbose_name = _('Title'), max_length = 255)
    title_english = models.CharField(verbose_name="Название отеля на английском", max_length=55)
    slug = models.SlugField(verbose_name = _('Url'))
    about = FroalaField(verbose_name = _('About'))
    about_en = FroalaField(verbose_name="Описание отеля на англиском")
    min_cost = models.IntegerField(verbose_name = _('Minimal cost'))
    cost = models.IntegerField(verbose_name = _('Cost'))
    category = models.ForeignKey(Category, verbose_name = _('Category'), on_delete = models.CASCADE)
    img = models.ImageField(verbose_name = _('Image'), upload_to = 'uploads/hotel')
    RESTS = (
        ('w', _('Winter')),
        ('s', _('Summer'))
    )
    rest = models.CharField(verbose_name = _('Type of rest'), max_length = 1, choices = RESTS)
    latitude = models.CharField(verbose_name="Широта", max_length=20)
    longitude = models.CharField(verbose_name="Долгота", max_length=20)
    related = models.ManyToManyField('self', blank=True, null=True)

    def __unicode__(self):
        return self.title

    class Meta:
        db_table = 'hotel'
        verbose_name = _('Hotel')
        verbose_name_plural = _('Hotels')


class Image(models.Model):
    image = models.ImageField(verbose_name = _('Image'), upload_to = 'uploads/hotel/images')
    hotel = models.ForeignKey(Hotel, verbose_name="К какому отелю относится")

    def pic(self):
        if self.image:
            return '<img src="%s" width="70">' %self.image.url
        else:
            return '(none)'
    pic.short_description = _('Big Image')
    pic.allow_tags = True

    class Meta:
        db_table = 'hotel_images'


class Hull(models.Model):
    title = models.CharField(verbose_name = "Название корпуса", max_length = 255)
    hull_en_title = models.CharField(verbose_name="Название корпуса на английском", max_length=55)

    def __unicode__(self):
        return self.title

    class Meta:
        db_table = 'hull'
        verbose_name = _('Hull')
        verbose_name_plural = "Корпусы"



class Hostel(models.Model):
    hull = models.ForeignKey(Hull, on_delete = models.CASCADE)
    title = models.CharField(verbose_name = _('Title'), max_length = 255)
    title_en = models.CharField(verbose_name="Название на английском", max_length=55, default=None)
    cost = models.CharField(verbose_name = _('Field for cost'), max_length = 255)
    cost_en = models.CharField(verbose_name=" Цена на английском", max_length=55, default=None)
    note = models.TextField(verbose_name="Заметки")
    note_en = models.TextField(verbose_name="Заметки на англиском", default=None)

    def __unicode__(self):
        return self.title

    class Meta:
        db_table = 'hostel'


class Tab(models.Model):
    title = models.CharField(verbose_name = _('Title'), max_length = 255)
    title_en = models.CharField(verbose_name="Название на английском", max_length=55)
    hotel = models.ForeignKey(Hotel, on_delete = models.CASCADE)
    hostel = models.ManyToManyField(Hostel)

    def __unicode__(self):
        return self.title

    class Meta:
        db_table = 'tab'
        verbose_name = _('Проживание')
        verbose_name_plural = "Проживание"
