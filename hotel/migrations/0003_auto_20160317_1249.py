# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-03-17 12:49
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hotel', '0002_auto_20160314_0557'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='tab',
            options={'verbose_name': '\u041f\u0440\u043e\u0436\u0438\u0432\u0430\u043d\u0438\u0435', 'verbose_name_plural': '\u041f\u0440\u043e\u0436\u0438\u0432\u0430\u043d\u0438\u0435'},
        ),
    ]
