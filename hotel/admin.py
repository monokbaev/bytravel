from django.contrib import admin
from .models import *


class HotelAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title', )}
    list_display = ('title', 'slug',)


class CategoryEnInline(admin.StackedInline):
    model = CategoryEn
    extra = 1

class CategoryAdmin(admin.ModelAdmin):
    inlines = [CategoryEnInline, ]

admin.site.register(Hostel)
admin.site.register(Hull)
admin.site.register(Category, CategoryAdmin)
admin.site.register(CategoryEn)
admin.site.register(Image)
admin.site.register(Tab)
admin.site.register(Hotel, HotelAdmin)