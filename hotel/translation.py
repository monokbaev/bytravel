#coding: utf-8
from modeltranslation.translator import translator, TranslationOptions
from .models import Hotel


class HotelTranslationOptions(TranslationOptions):
    fields = ('title',)

translator.register(Hotel, HotelTranslationOptions)