# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-03-07 13:30
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contacts', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='contact',
            old_name='name',
            new_name='name1',
        ),
        migrations.RenameField(
            model_name='contact',
            old_name='phone',
            new_name='phone1',
        ),
    ]
