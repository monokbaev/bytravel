# coding=utf-8
from __future__ import unicode_literals
from django.utils.translation import ugettext as _
from django.db import models

# Create your models here.


class Contact(models.Model):
    class Meta:
        verbose_name_plural = "Контакт"

    name1 = models.CharField(_("Имя контакта"), max_length=50)
    phone1 = models.CharField(_("Номер телефона"), max_length=50)

    def __unicode__(self):
        return self.name1
