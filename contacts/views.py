# coding=utf-8
from django.core.mail import send_mail
from django.shortcuts import render, HttpResponse, render_to_response
from django.template.context_processors import csrf

from .models import Contact

# Create your views here.


def csrf_view(request, context={}):
    context.update(csrf(request))
    return render_to_response('footer.html', context)


def contact_create(request):
    if request.method == 'POST':
        name1 = request.POST.get('name1')
        phone1 = request.POST.get('phone1')
        contact = Contact(name1=name1, phone1=phone1)
        contact.save()
        name1 = name1.encode('utf-8')
        name1 = 'Мое имя: ' + str(name1) + '\n'
        phone1 = 'Мой номер: ' + str(phone1) + '\n'
        contact_id = str(contact.id)
        send_mail("Заявка из сайта ByTravel", name1 + phone1 + 'Заявка номер: ' + contact_id, 'monokbaev@gmail.com', ['monokbaev@gmail.com'])
        return HttpResponse("Ваша заявка принята, мы перезвоним вам в самое ближайшее время")