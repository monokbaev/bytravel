from django.shortcuts import render, render_to_response

# Create your views here.
from django.views.generic import DetailView

from byTravel.models import Social, AddressEn, Phone, Mail, AboutEn, MainTextEn, Address, GlobalMeta, LocalMeta
from navbar.models import NavbarEn, Navbar
from slider.models import Slider
from hotel.models import Category, CategoryEn, Hotel, Image, Tab, Hull


def index_en(request, context={}):
    context['sliders'] = Slider.objects.all()
    context['socials'] = Social.objects.all()
    context['addresses'] = AddressEn.objects.all()
    context['phones'] = Phone.objects.all()
    context['mails'] = Mail.objects.all()
    context['navbars'] = NavbarEn.objects.all()
    context['category_list'] = CategoryEn.objects.all()
    context['main_text'] = MainTextEn.objects.get()
    context['gmeta'] = GlobalMeta.objects.filter(title="global")
    return render_to_response('main_en.html', context)


def about_en(request, context={}):
    context['sliders'] = Slider.objects.all()
    context['socials'] = Social.objects.all()
    context['addresses'] = AddressEn.objects.all()
    context['phones'] = Phone.objects.all()
    context['mails'] = Mail.objects.all()
    context['navbars'] = NavbarEn.objects.all()
    context['about'] = AboutEn.objects.get(id=2)
    context['gmeta'] = GlobalMeta.objects.filter(title="global")
    context['lmeta'] = GlobalMeta.objects.filter(title="about")
    return render_to_response('byTravel/about_en.html', context)


def transfer_en(request, context={}):
    context['sliders'] = Slider.objects.all()
    context['socials'] = Social.objects.all()
    context['addresses'] = AddressEn.objects.all()
    context['phones'] = Phone.objects.all()
    context['mails'] = Mail.objects.all()
    context['navbars'] = NavbarEn.objects.all()
    context['transfer'] = AboutEn.objects.get(id=1)
    context['gmeta'] = GlobalMeta.objects.filter(title="global")
    context['lmeta'] = GlobalMeta.objects.filter(title="transfer")
    return render_to_response('byTravel/transfer_en.html', context)


def winter_list_en(request, context={}):
    context['sliders'] = Slider.objects.all()
    context['socials'] = Social.objects.all()
    context['addresses'] = AddressEn.objects.all()
    context['phones'] = Phone.objects.all()
    context['mails'] = Mail.objects.all()
    context['navbars'] = NavbarEn.objects.all()
    context['category_list'] = CategoryEn.objects.all()
    context['hotels'] = Hotel.objects.filter(rest='w')
    context['gmeta'] = GlobalMeta.objects.filter(title="global")
    context['lmeta'] = GlobalMeta.objects.filter(title="winter")
    return render_to_response('hotel/winter_en.html', context)


def summer_list_en(request, context={}):
    context['sliders'] = Slider.objects.all()
    context['socials'] = Social.objects.all()
    context['addresses'] = AddressEn.objects.all()
    context['phones'] = Phone.objects.all()
    context['mails'] = Mail.objects.all()
    context['navbars'] = NavbarEn.objects.all()
    context['category_list'] = CategoryEn.objects.all()
    context['hotels'] = Hotel.objects.filter(rest='s')
    context['gmeta'] = GlobalMeta.objects.filter(title="global")
    context['lmeta'] = GlobalMeta.objects.filter(title="summer")
    return render_to_response('hotel/summer_en.html', context)


# def winter_detail_en(request, context={}, slug=""):
#     context['socials'] = Social.objects.all()
#     context['addresses'] = AddressEn.objects.all()
#     context['phones'] = Phone.objects.all()
#     context['mails'] = Mail.objects.all()
#     context['navbars'] = NavbarEn.objects.all()
#     context['category_list'] = CategoryEn.objects.all()
#     context['hotel'] = Hotel.objects.get(slug=slug)
#     context['images'] = Image.objects.filter(hotel__slug=slug)
#     context['tabs'] = Tab.objects.filter(hotel__slug=slug)
#     return render_to_response('hotel/winter_detail_en.html', context)


class WinterDetailView(DetailView):
    model = Hotel

    def get_context_data(self, **kwargs):
        context = super(WinterDetailView, self).get_context_data(**kwargs)
        context['images'] = Image.objects.all().filter(hotel = self.object)
        context['tabs'] = Tab.objects.all().filter(hotel = self.object)
        context['hulls'] = Hull.objects.all()
        context['navbars'] = NavbarEn.objects.all()
        for tab in context['tabs']:
            for hull in context['hulls']:
                hull.count = False
                for hostel in tab.hostel.all():
                    if hostel.hull.id == hull.id:
                        hull.count = True
        context['socials'] = Social.objects.all()
        context['addresses'] = AddressEn.objects.all()
        context['phones'] = Phone.objects.all()
        context['mails'] = Mail.objects.all()
        context['gmeta'] = GlobalMeta.objects.filter(title="global")
        context['lmeta'] = LocalMeta.objects.filter(hotel=self.object)

        return context
    template_name = 'hotel/winter_detail_en.html'