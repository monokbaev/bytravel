from django.conf.urls import url
from .views import *

app_name = 'hotel'
urlpatterns = [
    url(r'^hotel/winter/(?P<slug>[-_\w]+)$', WinterDetailView.as_view()),
    url(r'^hotel/winter/$', 'en.views.winter_list_en'),
    url(r'^hotel/summer/$', 'en.views.summer_list_en'),
    url(r'^transfer/$', 'en.views.transfer_en'),
    url(r'^about/$', 'en.views.about_en'),
    url(r'^$', 'en.views.index_en'),
]