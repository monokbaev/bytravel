# coding=utf-8
from __future__ import unicode_literals
from django.utils.translation import ugettext as _
from django.db import models

# Create your models here.


class Navbar(models.Model):
    class Meta:
        verbose_name_plural = "Пункты навигационного меню"

    title = models.CharField(_("Название пункта"), max_length=50)
    link = models.CharField(_("Ссылка куда ведет"), max_length=50)

    def __unicode__(self):
        return self.title


class NavbarEn(models.Model):
    class Meta:
        verbose_name_plural = "Пункты навигационногго меню на анлийском"

    title = models.CharField(_("Название пункта"), max_length=50)
    link = models.CharField(_("Ссылка куда ведет"), max_length=50)
    belongs_to = models.ForeignKey(Navbar)

    def __unicode__(self):
        return self.title
