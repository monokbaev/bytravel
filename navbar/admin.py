from django.contrib import admin
from .models import *
# Register your models here.


class NavbarInline(admin.StackedInline):
    model = NavbarEn
    extra = 1

class NavbarAdmin(admin.ModelAdmin):
    inlines = [NavbarInline, ]

admin.site.register(Navbar, NavbarAdmin)